#include "mz3class.h"
#include <QFile>
#include <QTextStream>

MZ3Class::MZ3Class()
{
    file = new QFile("C:\\QtProjects\\paintvariant.cpp");
    StringsNumber = 0;
    EmptyStringsNumber = 0;
    CommentStringsNumber = 0;
    CodeCommentStringsNumber = 0;
    UniqueOperatorsNumber = 0;
    UniqueOperandsNumber = 0;
    OperatorsNumber = 0;
    OperandsNumber = 0;
    SeparatorsNumber = 0;
    MaxLevel = 0;
}

void  MZ3Class::initOperators()
{
    int i = 0;
    QFile op("C:\\QtProjects\\operators.txt");

    if (op.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&op);
        while (!in.atEnd())
        {
            Operators.push_back(in.readLine());
            i++;
        }
        op.close();
    }
}

void  MZ3Class::calcStringsNumber()
{
    if (file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
       StringsNumber = QTextStream(file).readAll().split('\n').count();
       file->close();
    }

}

void  MZ3Class::calcCommentStringsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.trimmed().startsWith("//"))
               CommentStringsNumber++;
        }
        file->close();
    }
}

void  MZ3Class::calcOperandsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList s;
            if(line.contains("+"))
            {
                s = line.split("+");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("-"))
            {
                s = line.split("-");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("*"))
            {
                s = line.split("*");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("/"))
            {
                s = line.split("/");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }
            if(line.contains("="))
            {
                s = line.split("=");
                if (!UniqueOperands.contains(s[0]))
                    UniqueOperands.append(s[0]);
                if (!UniqueOperands.contains(s[1]))
                    UniqueOperands.append(s[1]);
                OperandsNumber+=2;
            }

             UniqueOperandsNumber = UniqueOperands.toSet().count();
        }
        file->close();
    }
}

void  MZ3Class::calcEmptyStringsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if(line.trimmed().isEmpty())
                EmptyStringsNumber++;
        }
        file->close();
    }
}

void  MZ3Class::calcCodeCommentStringsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("//"))
               CodeCommentStringsNumber++;
        }
        file->close();
    }
}

void  MZ3Class::calcOperatorsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            for (int i = 0; i < Operators.toSet().count(); i++)
            {
                if (line.contains(Operators[i]))
                {
                    OperatorsNumber++;
                    if(!UniqueOperators.contains(Operators[i]))
                        UniqueOperators.append(Operators[i]);
                }
            }
            UniqueOperatorsNumber = UniqueOperators.toSet().count();
        }
        file->close();
    }
}

void  MZ3Class::calcSeparatorsNumber()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains(";"))
               SeparatorsNumber++;
        }
        file->close();
    }
}

void  MZ3Class::calcMaxLevel()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("if") || line.contains("else") ||
                line.contains("else if") || line.contains("goto") ||
                line.contains("case"))
               MaxLevel++;
        }
        file->close();
    }
}

QString  MZ3Class::TotalCalculation()
{
    initOperators();
    calcStringsNumber();
    calcEmptyStringsNumber();
    calcCommentStringsNumber();
    calcCodeCommentStringsNumber();

    calcOperatorsNumber();
    calcOperandsNumber();

    calcSeparatorsNumber();
    calcMaxLevel();

    QString total = "";
    total+= QString::number(StringsNumber); total+= " "; //Количество строк кода
    total+=QString::number(EmptyStringsNumber); total+= " "; //Количество пустых строк кода
    total+=QString::number(CommentStringsNumber); total+= " "; //Количество строк с комментариями
    total+=QString::number(CodeCommentStringsNumber); total+= " "; //Количество строк с кодом и комментариями
    total+=QString::number(CommentStringsNumber*100/StringsNumber); total+= " "; //Процент комментариев

    total+=QString::number(UniqueOperatorsNumber+SeparatorsNumber); total+= " "; //Словарь операторов
    total+=QString::number(UniqueOperandsNumber); total+= " "; //Словарь операндов
    total+=QString::number(OperatorsNumber); total+= " "; //Общее количество операторов
    total+=QString::number(OperandsNumber); total+= " "; //Общее количество операндов

    total+=QString::number(OperandsNumber+OperatorsNumber); total+= " "; //Длина программы
    total+=QString::number((OperandsNumber+OperatorsNumber)*log2(UniqueOperandsNumber+UniqueOperatorsNumber)); total+= " "; //Объем программы
    total+=QString::number(MaxLevel);   //Цикломатическая сложность программы

    return total;
}
